package com.example.josias.exemplorv.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.eqs.exemplointenty.R

import com.example.jonathawindows.exemplorv.models.Evento
import kotlinx.android.synthetic.main.list_eventos.view.*

class MyAdapter (val context:Context,
                 val eventos:ArrayList<Evento>,
                 var clickListener:(Evento)->Unit
                ) : RecyclerView.Adapter<MyAdapter.ViewHolder>()
{
    class ViewHolder(itemView:View):
            RecyclerView.ViewHolder(itemView)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var v = LayoutInflater.from(parent.context)
                .inflate(R.layout.list_eventos, parent, false)
        var vh = ViewHolder(v)
        return vh
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var evento = eventos[position]
        holder.itemView.tvNome.text = evento.nome
        holder.itemView.tvDesc.text = evento.desc

        holder.itemView.setOnClickListener{clickListener(evento)}
    }
    override fun getItemCount(): Int {
        return eventos.size
    }
}