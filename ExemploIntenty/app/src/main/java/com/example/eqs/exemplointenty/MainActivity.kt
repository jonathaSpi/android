package com.example.eqs.exemplointenty

import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun openURL(v:View) {
        var i= Intent(Intent.ACTION_VIEW,
                Uri.parse("http://www.google.com")
        )
        startActivity(i)
    }
    fun openNewScreen(v:View) {

        SessionController.str1 = "Teste 01"
        SessionController.str2 = "Teste 02"


        var i= Intent(
                this,
                SecondActivity::class.java
        )
        startActivity(i)

    }



}
