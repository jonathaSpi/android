package com.example.eqs.exemplointenty

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View

class SecondActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)



        Log.i("Teste",SessionController.str1)
        Log.i("Teste",SessionController.str2)


    }
    fun openMain(v: View) {

        SessionController.str1 = "Teste 01"
        SessionController.str2 = "Teste 02"


        var i = Intent(
                this,
                ServicoActivity::class.java
        )
        startActivity(i)
    }

}
